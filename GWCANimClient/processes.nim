import os
import strutils
import times
import winlean
import winextra

# =============================================================================
# Get PID from process name
# =============================================================================
proc getPidFromProcessName*( processName: string ): DWORD = 
    var
        hProcessSnap : Handle         = 0
        pe32         : PROCESSENTRY32 = PROCESSENTRY32()

    hProcessSnap = createToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 )

    if hProcessSnap == INVALID_HANDLE_VALUE: return 0

    pe32.dwSize = sizeof( PROCESSENTRY32 ).DWORD

    if not process32First(hProcessSnap, addr(pe32)):
        discard closeHandle( hProcessSnap )
        echo "Failed to gather system information!"

    if pe32.szExeFile == processName:
        discard closeHandle(hProcessSnap)
        return pe32.th32ProcessID

    while process32Next(hProcessSnap, addr(pe32)):
        if pe32.szExeFile == processName:
            discard closeHandle(hProcessSnap)
            return pe32.th32ProcessID

proc getAllPidsFromProcessName*( processName: string ): seq[int32] = 
    var
        hProcessSnap : Handle         = 0
        pe32         : PROCESSENTRY32 = PROCESSENTRY32()

    hProcessSnap = createToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 )

    if hProcessSnap == INVALID_HANDLE_VALUE: return

    pe32.dwSize = sizeof( PROCESSENTRY32 ).DWORD

    if not process32First(hProcessSnap, addr(pe32)):
        discard closeHandle( hProcessSnap )
        echo "Failed to gather system information!"

    result = newSeq[int32]()
    if pe32.szExeFile == processName:
        result.add( pe32.th32ProcessID.int32 )

    while process32Next(hProcessSnap, addr(pe32)):
        if pe32.szExeFile == processName:
            result.add( pe32.th32ProcessID.int32 )

    discard closeHandle(hProcessSnap)

# =============================================================================
# Get Handle From PID
# =============================================================================
type
    pidData = ref object
        processID    : DWORD
        handleBuffer : Handle

proc cbFromPID( handle: Handle, adata: ptr DWORD ): bool {.stdcall.} =
    var
        data = cast[ptr pidData](adata)
        pid: DWORD = 0
    discard getWindowThreadProcessId( handle, pid.addr ) 
    if data.processID == pid:
        if getWindow( handle, 4 ) == 0:
            if isWindowVisible( handle ):
                data.handleBuffer = handle
                return false
    return true

proc getHandleFromPid*( pid: DWORD ): Handle =
    var data = pidData( processID:pid, handleBuffer: 0)
    discard enumWindows( cbFromPID, cast[ptr DWORD](data.addr) )
    return data.handleBuffer

# =============================================================================
# Get Handle from Window Title
# =============================================================================
proc getHandleFromTitle*( title: string ): Handle =
    return findWindowA( nil, title )

proc getWindowTitle*( handle: Handle ): string =
    var title: array[256, char] = (new array[256, char])[]
    discard getWindowTextA( handle, title, sizeof(title).int32 )
    return $title

# =============================================================================
# Get Handle From Character Name
# - Checks all titles to see if they have the character name.
# =============================================================================
type
    titleData = ref object
        partial      : string
        handleBuffer : Handle

proc cbFromTitleString( handle: Handle, adata: ptr DWORD ): bool {.stdcall.} =
    var data = cast[ptr titleData](adata)
    if getWindow( handle, 4 ) == 0:
        if isWindowVisible( handle ):
            # See if the window title contains our character name!
            if getWindowTitle( handle ).contains( data.partial ):
                data.handleBuffer = handle
                return false
    return true

proc getHandleFromPartialString*( partial: string ): Handle =
    var data = titleData( partial:partial, handleBuffer: 0 )
    discard enumWindows( cbFromTitleString, cast[ptr DWORD](data.addr) )
    return data.handleBuffer

# =============================================================================
# Get PID from Handle
# =============================================================================
proc getPidFromHandle*( handle: Handle ): int32 =
    result = 0
    discard getWindowThreadProcessId( handle, result.addr )

# =============================================================================
# GetModuleInformation
# =============================================================================
# Note this needs the handle you get from openProcess!
proc getModuleHandle*( hMemory: Handle, processName: string ): Handle =
    var
        hArray: array[100, Handle] #= (new array[100, Handle])[]
        count: int32 = 0
        correctIndex = -1
        fName: string = ""

    discard enumProcessModules( hMemory, hArray.addr.pointer, sizeof(hArray), count.addr )

    for i in 0..((count.int32/sizeof(int32)).int32 - 1):
        discard getModuleFileNameEx( hMemory, hArray[i], fName, count )
        var name = $fname.cstring # we need to have a workable string again.

        # echo name
        # echo processName
        # echo name[name.len-processName.len..name.len-1]
        if name[name.len-processName.len..name.len-1] == processName:
            correctIndex = i
            break

    if correctIndex == -1: raise newException( Exception, "Could not find module!" )

    result = hArray[correctIndex]

# =============================================================================
# Dump Memory
# =============================================================================
proc dumpMemory*( hMemory: Handle, info: MODULEINFO ): seq[byte] = 
    result = newSeq[byte](info.SizeOfImage)
    var
        read:int32 = -1

        bRead = readProcessMemory( hMemory, info.lpBaseOfDll, result[0].addr, info.SizeOfImage, read.addr )

    if bRead == 0: raise newException( Exception, "[dumpMemory] Could not read memory!" )

# =============================================================================
# Find Pattern in Memory
# =============================================================================
proc findPattern*( memory, pattern: openarray[byte], mask: string, offset: int32 ): int32 =

    if pattern.len != mask.len: return -1

    for i in 0..memory.len-1:

        for x in 0..pattern.len-1:

            if mask[x] == '?': continue

            if pattern[x] != memory[i + x]: break

            if x == pattern.len-1: return (i.int32 + offset)

# =============================================================================
# Reading Pointers
# =============================================================================
proc readPointer*[T]( hMemory: Handle, address: int32 ): T =
    var read: int32 = -1
    discard readProcessMemory( hMemory, address, result.addr, sizeof(result).int32, read.addr )

proc readPointerChain*[T]( hMemory: Handle, baseAddress: int32, offsets: openarray[int32], finalOffset: int32 = 0 ): T =
    var
        address = baseAddress
        read: int32 = -1

    for offset in offsets:
        # echo "Address: ", toHex(address)
        # echo "Offset: ", toHex(offset)
        # echo "Address + Offset: ", toHex(address + offset)
        discard readProcessMemory( hMemory, address + offset, address.addr, sizeof(address).int32, read.addr )
        # echo "\n"

    discard readProcessMemory( hMemory, address + finalOffset, result.addr, sizeof(result).int32, read.addr )


# =============================================================================
# Inject DLL
# =============================================================================
proc injectDLL*( hMemory: Handle, dll: string ): int32 =
    var
        dllPath   = os.expandFilename( dll ).cstring
        pLoadLib  = getProcAddress( getModuleHandleA( "kernel32.dll" ), "LoadLibraryA" )
        pBuffer   = virtualAllocEx( hMemory, 0, (dllPath.len + 1).int32, (MEM_RESERVE or MEM_COMMIT), PAGE_READWRITE )

    discard writeProcessMemory( hMemory, pBuffer, dllPath, (dllPath.len + 1).int32, nil )

    var hThread = createRemoteThread( hMemory, nil, 0, pLoadLib, pBuffer, 0, nil )

    discard waitForSingleObject( hThread, -1 )
    discard virtualFreeEx( hMemory, pBuffer, 0, MEM_RELEASE )

    discard getExitCodeThread( hThread, result.addr ).bool

    discard closeHandle( hThread )

# hDll is the result from injectDLL
proc ejectDLL*( hMemory: Handle, hDll: Handle ): int32 =
    var dll = hDll

    var
        hKernel  = getModuleHandleA( "kernel32.dll" )
        pFreeLib = getProcAddress( hKernel, "FreeLibrary" )
        pBuffer  = virtualAllocEx( hMemory, 0, sizeof(dll).int32, (MEM_RESERVE or MEM_COMMIT), PAGE_READWRITE )

    echo toHex(hKernel)
    echo repr(pFreeLib)
    echo repr(pBuffer)

    echo writeProcessMemory( hMemory, pBuffer, dll.addr, sizeof(dll).int32, nil )

    var hThread  = createRemoteThread( hMemory, nil, 0, pFreeLib, pBuffer, 0, nil )
    echo repr(hThread)

    echo waitForSingleObject( hThread, -1 )

    echo getExitCodeThread( hThread, result.addr ).bool

    echo closeHandle( hThread )


export winextra

when isMainModule:
    # var
        # pid    = getPidFromProcessName("Gw.exe")
        # handle  = getHandleFromPartialString( "Yaen The Blacksmith" )

    # discard pid.injectDLL( "..\\Debug\\GWCAServer.dll" )

    var
        pid     = getPidFromProcessName( "Gw.exe" )
        hMemory = openProcess( PROCESS_ALL_ACCESS, false, pid )
        hModule = getModuleHandle( hMemory, "GWCAServer.dll" )
        
    #     hModule = getModuleHandle( hMemory, "Gw.exe" )
    #     info    = MODULEINFO()
    # discard getModuleInformation( hMemory, hModule, info.addr, sizeof(info).int32 )
    # var
    #     meminfo = MEMORY_BASIC_INFORMATION()
    #     searchaddr = info.lpBaseOfDll


    echo toHex(hModule)

    # echo ejectDLL( pid, hModule, hMemory )

    # while searchaddr < info.lpBaseOfDll + info.SizeOfImage:

    #     discard virtualQueryEx( hMemory, searchaddr, meminfo.addr, sizeof(meminfo).int32 )

    #     echo repr( meminfo )
    #     var
    #         dump = newSeq[byte]( meminfo.RegionSize )
    #         read: int32 = -1

    #     discard readProcessMemory( hMemory, searchaddr, dump[0].addr, meminfo.RegionSize, read.addr )

    #     var address = findPattern( dump, [0x56'u8,0x8B,0xF1,0x3B,0xF0,0x72,0x04], "xxxxxxx", 0xC'i32 )

    #     if address != 0:
    #         echo toHex( searchaddr + address )

    #         var tmp: int32

    #         discard readProcessMemory( hMemory, searchaddr + address, tmp.addr, sizeof(tmp).int32, read.addr )

    #         echo "AgentArrayAddress => ", tmp.toHex

    #         var playerID, targetID: byte

    #         echo toHex(tmp - 0x054'i32)
    #         echo toHex(tmp - 0x500'i32)

    #         var t = cpuTime()
    #         for i in 0..10_000:
    #             discard readProcessMemory( hMemory, tmp - 0x054'i32, playerID.addr, sizeof(playerID).int32, read.addr )
    #             discard readProcessMemory( hMemory, tmp - 0x500'i32, targetID.addr, sizeof(targetID).int32, read.addr )

    #         echo "10,000 Memory Reads Time Taken: ", cpuTime() - t

    #         echo "PlayerID => ", playerID
    #         echo "TargetID => ", targetID

    #     searchaddr = searchaddr + meminfo.RegionSize



    #     dump    = dumpMemory( hMemory, info )

    #     agentArray = info.lpBaseOfDll + findPattern( dump, [0x56'u8,0x8B,0xF1,0x3B,0xF0,0x72,0x04], "xxxxxxx", 0xC'i32 )

    #     playerID: byte
    #     read: int32

    # echo toHex(agentArray - 0xC'i32)
    # echo toHex(agentArray - 0x54'i32)
    # echo toHex(agentArray - 0x500'i32)

    # echo readProcessMemory( hMemory, agentArray - 0x54'i32, playerID.addr, sizeof(playerID).int32, read.addr )
    # echo playerID

    # echo repr( dump )
    # echo repr( dump.addr )
    # echo repr( dump[0].addr )

    # echo repr(info.lpBaseOfDll)
    # echo cast[int32](info.lpBaseOfDll)

    # var data = window_data(processID:123)
    # var p: DWORD = cast[DWORD](data.addr)
    # echo repr(data)
    # echo repr(p)
    # echo repr(p[])
    # echo repr(p[].addr)

    # var pid = getPidFromProcessName( "Gw.exe" )
    # echo "PID   : ", pid

    # var handle = getHandleFromPid( pid )
    # echo "handle   -> ", handle
    # discard getWindowThreadProcessId( handle, pid.addr )
    # echo "PID      -> ", pid

    # var handle = openProcess( PROCESS_ALL_ACCESS, false, pid )
    # echo "Skype - Handle: ", handle

    # var handle = getHandleFromTitle( "Yaen The Blacksmith" )
    # echo "handle   -> ", handle
    # discard getWindowThreadProcessId( handle, pid.addr )
    # echo "PID      -> ", pid
    # discard injectDLL( handle, "..\\Debug\\GWCAServer.dll" )

    # var title: array[256, char]
    # echo getWindowTextA( handle, title, sizeof(title).int32 )
    # echo title.len
    # echo title
