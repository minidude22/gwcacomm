import winlean

const
    TH32CS_SNAPPROCESS* = 0x00000002
    PROCESS_ALL_ACCESS* = 0x001F0FFF'i32
    MEM_COMMIT*         = 0x1000'i32
    MEM_RESERVE*        = 0x2000'i32
    MEM_RELEASE*        = 0x8000'i32

type
    PROCESSENTRY32* {.final, pure.} = object
        dwSize*              : int32
        cntUsage*            : int32
        th32ProcessID*       : int32
        th32DefaultHeapID*   : ULONG_PTR
        th32ModuleID*        : int32
        cntThreads*          : int32
        th32ParentProcessID* : int32
        pcPriClassBase*      : LONG
        dwFlags*             : int32
        szExeFile*           : array[0..(MAX_PATH) - 1, char]

    MODULEINFO* {.final, pure.} = object
        lpBaseOfDll* : int32
        SizeOfImage* : int32
        EntryPoint*  : pointer

    MEMORY_BASIC_INFORMATION* {.final, pure.} = object
        BaseAddress*       : pointer
        AllocationBase*    : pointer
        AllocationProtect* : int32
        RegionSize*        : int32
        State*             : int32
        Protect*           : int32
        Type*              : int32

proc setNamedPipeHandleState*(hNamedPipe: Handle, lpMode, lpMaxCollectionCount, lpCollectionDataTimeout: ptr DWORD): bool {.
    stdcall, importc:"SetNamedPipeHandleState", dynlib:"kernel32".}

proc transactNamedPipe*(hNamedPipe: Handle, lpInBuffer: pointer, nInBufferSize: int32,
                       lpOutBuffer: pointer, nOutBufferSize: int32, lpBytesRead: ptr int32,
                       lpOverlapped: pointer ): WINBOOL {.stdcall,
    dynlib: "kernel32", importc: "TransactNamedPipe".}

proc createToolhelp32Snapshot*(dwFlags, th32ProcessID: int32): Handle {.
    stdcall, importc: "CreateToolhelp32Snapshot", dynlib: "kernel32".}

proc process32First*(hSnapshot: Handle, lppe: ptr PROCESSENTRY32): bool {.
    stdcall, importc: "Process32First", dynlib: "kernel32".}

proc process32Next*(hSnapshot: Handle, lppe: ptr PROCESSENTRY32): bool {.
    stdcall, importc: "Process32Next", dynlib: "kernel32".}

proc getWindowTextA*(hWnd: Handle, lpString: cstring, nMaxCount: int32): int32 {.
    stdcall, importc: "GetWindowTextA", dynlib: "user32".}

proc enumWindows*(lpEnumFunc: proc(handle: Handle, data: ptr int32): bool{.stdcall.}, lp: ptr int32): bool {.
    stdcall, importc: "EnumWindows", dynlib: "user32".}

proc getWindowThreadProcessId*(hWnd: Handle, lpdwProcessId: ptr int32): int32 {.
    stdcall, importc: "GetWindowThreadProcessId", dynlib: "user32".}

proc getWindow*(hWnd: Handle, uCmd: int32 ): Handle {.stdcall, importc: "GetWindow", dynlib: "user32".}

proc isWindowVisible*(hWnd: Handle): bool {.
    stdcall, importc: "IsWindowVisible", dynlib: "user32".}

proc findWindowA*(lpClassName: cstring, lpWindowName: cstring): Handle {.
    stdcall, importc: "FindWindowA", dynlib: "user32".}

proc openProcess*(dwDesiredAcces: int32, bInheritHandle: bool, dwProcessID: int32): Handle {.
    stdcall, importc: "OpenProcess", dynlib: "kernel32".}

proc enumProcessModules*( hProcess: Handle, lphModule: pointer, size: int, lpcbNeeded: ptr int32 ): int32 {.
    stdcall, importc: "EnumProcessModules", dynlib: "psapi.dll".}

proc getModuleInformation*( hProcess, hModule: Handle, lpmodinfo: ptr MODULEINFO, size: int32 ): int32 {.
    stdcall, importc: "GetModuleInformation", dynlib: "psapi.dll".}

proc getModuleFileNameEx*( hProcess, hModule: Handle, lpFilename: cstring, size: int32 ): int32 {.
    stdcall, importc: "GetModuleFileNameExA", dynlib: "psapi.dll".}

proc readProcessMemory*(hProcess: Handle, lpBaseAddress: int32, lpBuffer: pointer,
                        nSize: int32, lpNumberOfBytesRead: ptr int32): int32 {.
    stdcall, importc: "ReadProcessMemory", dynlib: "kernel32".}

proc writeProcessMemory*(hProcess: Handle, lpBaseAddress, lpBuffer: pointer,
                        nSize: int32, lpNumberOfBytesWritten: ptr int32 ): int32 {.
    stdcall, importc: "WriteProcessMemory", dynlib: "kernel32".}

proc getModuleHandleA*(lpModuleName: cstring): Handle {.
    stdcall, importc: "GetModuleHandleA", dynlib: "kernel32".}

proc getProcAddress*(hModule: Handle, lpProcName: cstring): pointer {.
    stdcall, importc: "GetProcAddress", dynlib: "kernel32".}

proc virtualAllocEx*(hprocess: Handle, lpAddress, dwSize, flAllocationType, flProtect: int32): pointer {.
    stdcall, importc: "VirtualAllocEx", dynlib: "kernel32".}

proc virtualFreeEx*(hprocess: Handle, lpAddress: pointer, dwSize, dwFreeType: int32): int32 {.
    stdcall, importc: "VirtualFreeEx", dynlib: "kernel32".}

proc virtualQueryEx*(hProcess: Handle, lpAddress: int32,
                     lpBuffer: ptr MEMORY_BASIC_INFORMATION, dwLength: int32): int32 {.
    stdcall, dynlib: "kernel32", importc: "VirtualQueryEx".}

proc createRemoteThread*(hProcess: Handle, lpThreadAttributes: pointer,
                        dwStackSize: int32, lpStartAddress, lpParameter: pointer,
                        dwCreationFlags: int32, lpThreadId: ptr int32): Handle {.
    stdcall, importc: "CreateRemoteThread", dynlib: "kernel32".}

proc getExitCodeThread*(hThread: Handle, lpExitCode: ptr int32): int32 {.
    stdcall, importc: "GetExitCodeThread", dynlib: "kernel32".}