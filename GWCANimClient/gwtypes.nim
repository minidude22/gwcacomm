type
    ServerCommand* {.pure.} = enum
        HEARTBEAT,
        DISCONNECT_PIPE_CONNECTION,
        UNLOAD_SERVER,

        REQUEST_BASE_POINTERS,

        MOVE, 
        USE_SKILL, 
        GET_SKILL_RECHARGE,
        GET_CAST_QUEUE_SIZE,
        GET_SKILL_ADRENALINE,

        GET_EFFECT_TIME_REMAINING,

        SEND_CHAT,

        GET_PLAYER_ID,
        GET_TARGET_ID,

        GET_PLAYER,
        GET_AGENT_BY_ID,
        GET_AGENTS_POS,
        GET_AGENT_POS,

        ALLOCATE_MEMORY,
        FREE_MEMORY,
    
        GET_MAP_ID,
        GET_MAP_INFO,
        GET_INSTANCE_TYPE,
        GET_INSTANCE_TIME,

        CHANGE_TARGET,

        GO_NPC,
        SEND_DIALOG,

        MAP_TRAVEL,
        MAP_TRAVEL_RANDOM,

        TRAVEL_GUILD_HALL,
        LEAVE_GUILD_HALL,

        GET_MAP_LOADED,

        CHANGE_SECONDARY,
        SET_ATTRIBUTES,
        LOAD_SKILLBAR,

        SET_RENDER_HOOK,
        SEND_PACKET,

        GET_CHARACTER_NAME,

        GET_AGENT_HANDLE,
        GET_FILTERED_AGENT_ARRAY,

        GET_ITEM_BY_SLOT,
        GET_ITEM_BY_AGENTID,
        GET_ITEMS_BY_MODELID,
        GET_ITEM_NAME,
  
        BUY_MERCHANT_ITEM,
        SELL_MERCHANT_ITEM,
        COLLECT_ITEM, 
        CRAFT_ITEM, 
        BUY_TRADER_ITEM, 
        SELL_TRADER_ITEM, 

        FIX_AGENT_POSITION, 
        GET_EFFECT_HANDLE, 
        GET_SKILLBAR_HANDLE,
        GET_SKILLBAR_INFO,

        INTERACT_AGENT,
        USE_TARGET_FUNC,

        COMMANDS_END

    SkillbarSkill* = object
        AdrenalineA*: int32
        AdrenalineB*: int32
        Recharge*   : int32
        SkillId*    : int32
        Event*      : int32

    Skillbar* = object
        AgentID* : int32
        Skills*  : array[8, SkillbarSkill]
        Disabled*: int32
        unknown1*: array[8, byte]
        Casting* : int32
        unknown2*: array[8, byte]

    gwArray* [T] = object
        array_ptr*: int32
        allocated_size*: int32
        current_size*: int32
        default_size*: int32

    Bag* = object
        unknown1*     : array[4, byte]
        index*        : int32
        BagId*        : int32
        ContainerItem*: int32
        ItemsCount*   : int32
        BagArray*     : int32 # Pointer to a bag
        Items*        : gwArray[int32] # Array of pointers to Item objects

    Item* = object
        ItemId*       : int32
        AgentId*      : int32
        unknown1*     : array[4, byte]
        bag*          : int32 # pointer to the bag he's in?
        ModStruct*    : int32 # pointer to an array of mods
        ModStructSize*: int32 # Size of the mod array
        Customized*   : cstring
        modelFileId*  : int32
        Type*         : byte
        unknown7*     : byte
        ExtraId*      : int16
        value*        : int16
        unknown4*     : array[4, byte]
        interaction*  : int16
        ModelId*      : int32
        modString*    : int32 # byte pointer
        unknown5*     : array[4, byte]
        ExtraItemInfo*: int32 # byte pointer
        unknown6*     : array[15, byte]
        Quantity*     : byte
        equipped*     : byte
        profession*   : byte
        slot*         : byte

# Get the memory location inside the gwArray based upon the index
proc `[]`*( arr: gwArray, index: int32 ): int32 =
    if index > arr.current_size:
        echo repr( arr )
        echo index * sizeof(arr.T).int32
        raise newException( IndexError, "[gwArray] Index out of bounds!" ) 
    result = arr.array_ptr + ( index * sizeof(arr.T).int32 )

# Is implicitly called when looping
# iterator items*( arr: gwArray ): arr.T =
#     var index = 0
#     while index >= arr.current_size:
#         yield arr[index.int32]
#         inc index

type Agent* = object
        vtable*             : int32
        unknown1*           : array[24, byte]
        unknown2*           : array[4, byte] # This actually points to the agent before but with a small offset
        NextAgent*          : int32 # Pointer to the next agent (by id)
        unknown3*           : array[8, byte]
        Id*                 : int32
        Z*                  : float32 # Z coord in float
        Width1*             : float32 # Width of the model's box
        Height1*            : float32 # Height of the model's box
        Width2*             : float32 # Width of the model's box (same as 1)
        Height2*            : float32 # Height of the model's box (same as 1)
        Width3*             : float32 # Width of the model's box (usually same as 1)
        Height3*            : float32 # Height of the model's box (usually same as 1)
        Rotation_angle*     : float32 # Rotation in radians from East (-pi to pi)
        Rotation_cos*       : float32 # cosine of rotation
        Rotation_sin*       : float32 # sine of rotation
        NameProperties*     : int32 # Bitmap basically telling what the agent is
        unknown4*           : array[8, byte]
        unkfloat1*          : float32 # weird values, change with movement, always between -1 and 1
        unkfloat2*          : float32
        unkfloat3*          : float32
        unknown5*           : array[4, byte]
        X*                  : float32 # X coord in float
        Y*                  : float32 # Y coord in float
        Ground*             : int32
        unknown6*           : array[4, byte]
        NameTagX*           : float32 # Exactly the same as X above
        NameTagY*           : float32 # Exactly the same as Y above
        NameTagZ*           : float32 # Z coord in float
        unknown7*           : array[12, byte]
        Type*               : int32 # 0xDB = players, npc's, monsters etc. 0x200 = signpost/chest/object (unclickable). 0x400 = item to pick up
        MoveX*              : float32 # If moving, how much on the X axis per second
        MoveY*              : float32 # If moving, how much on the Y axis per second
        unknown8*           : array[4, byte] # always 0?
        Rotation_cos2*      : float32 # same as cosine above
        Rotation_sin2*      : float32 # same as sine above
        unknown10*          : array[16, byte]
        Owner*              : int32
        ItemId*             : int32 # Only valid if agent is type 0x400 (item)
        unknown24*          : array[4, byte]
        ExtraType*          : int32
        unknown11*          : array[24, byte]
        WeaponAttackSpeed*  : float32 # The base attack speed in float of last attacks weapon. 1.33 = axe, sWORD, daggers etc.
        AttackSpeedModifier*: float32 # Attack speed modifier of the last attack. 0.67 = 33% increase (1-.33)
        PlayerNumber*       : int16 # Selfexplanatory. All non-players have identifiers for their type. Two of the same mob = same number
        unknown12*          : array[6, byte]
        Equip*              : int32 # Pointer to a pointer
        unknown13*          : array[10, byte]
        Primary*            : byte # Primary profession 0-10 (None,W,R,Mo,N,Me,E,A,Rt,P,D)
        Secondary*          : byte # Secondary profession 0-10 (None,W,R,Mo,N,Me,E,A,Rt,P,D)
        Level*              : byte # Duh!
        TeamId*             : byte # 0=None, 1=Blue, 2=Red, 3=Yellow
        unknown14*          : array[14, byte]
        Energy*             : float32 # Only works for yourself
        MaxEnergy*          : int32 # Only works for yourself
        unknown15*          : array[4, byte]
        HPPips*             : float32 # Regen/Degen as float
        unknown16*          : array[4, byte]
        HP*                 : float32 # Health in % where 1=100% and 0=0%
        MaxHP*              : int32 # Only works for yourself
        Effects*            : int32 # Bitmap for effects to display when targetted. DOES include hexes
        unknown17*          : array[4, byte]
        Hex*                : byte # Bitmap for the hex effect when targetted (apparently obsolete!) (yes)
        unknown18*          : array[18, byte]
        ModelState*         : int32 # Different values for different states of the model.
        TypeMap*            : int32 # Odd variable! 0x08 = dead, 0xC00 = boss, 0x40000 = spirit, 0x400000 = player
        unknown19*          : array[16, byte]
        InSpiritRange*      : int32 # Tells if agent is within spirit range of you. Doesn't work anymore?
        unknown20*          : array[16, byte]
        LoginNumber*        : int32 # Unique number in instance that only works for players
        AnimationSpeed*     : float32 # Speed of the current animation
        AnimationUnk*       : array[4, byte] # related to animations
        AnimationID*        : int32 # Id of the current animation
        unknown22*          : array[32, byte]
        DaggerStatus*       : byte # 0x1 = used lead attack, 0x2 = used offhand attack, 0x3 = used dual attack
        Allegiance*         : byte # 0x1 = ally/non-attackable, 0x2 = neutral, 0x3 = enemy, 0x4 = spirit/pet, 0x5 = minion, 0x6 = npc/minipet
        WeaponType*         : int16 # 1=bow, 2=axe, 3=hammer, 4=daggers, 5=scythe, 6=spear, 7=sWORD, 10=wand, 12=staff, 14=staff
        Skill*              : int16 # 0 = not using a skill. Anything else is the Id of that skill
        WeaponItemType*     : int16
        OffhandItemType*    : int16
        WeaponItemId*       : int16
        OffhandItemId*      : int16

proc health*        ( agent: Agent ): int32 = return (agent.HP * agent.MaxHP.float).int32
proc energy*        ( agent: Agent ): int32 = return (agent.Energy * agent.MaxEnergy.float).int32
proc isCasting*     ( agent: Agent ): bool = return agent.Skill != 0
proc isBleeding*    ( agent: Agent ): bool = return (agent.Effects and 0x0001 ) > 0
proc hasCondition*  ( agent: Agent ): bool = return (agent.Effects and 0x0002 ) > 0
proc isDead*        ( agent: Agent ): bool = return (agent.Effects and 0x0010 ) > 0
proc hasDeepWound*  ( agent: Agent ): bool = return (agent.Effects and 0x0020 ) > 0
proc isPoisoned*    ( agent: Agent ): bool = return (agent.Effects and 0x0040 ) > 0
proc isEnchanted*   ( agent: Agent ): bool = return (agent.Effects and 0x0080 ) > 0
proc hasDegenHex*   ( agent: Agent ): bool = return (agent.Effects and 0x0400 ) > 0
proc hasHex*        ( agent: Agent ): bool = return (agent.Effects and 0x0800 ) > 0
proc hasWeaponSpell*( agent: Agent ): bool = return (agent.Effects and 0x8000 ) > 0
proc isBoss*        ( agent: Agent ): bool = return (agent.TypeMap and 1024   ) > 0

type
    Effect* = object
        SkillId*   : int32   # skill id of the effect
        EffectType*: int32   # type classifier 0 = condition/shout, 8 = stance, 11 = maintained enchantment, 14 = enchantment/nature ritual
        EffectId*  : int32   # unique identifier of effect
        AgentId*   : int32   # non-zero means maintained enchantment - caster id
        Duration*  : float32 # non-zero if effect has a duration
        TimeStamp* : int32   # GW-timestamp of when effect was applied - only with duration

    Buff* = object
        SkillId*      : int32 # skill id of the buff
        Unknown1*     : int32 # 
        BuffId*       : int32 # id of buff in the buff array
        TargetAgentId*: int32 # agent id of the target (0 if no target)

    AgentEffects* = object
        AgentID*: int32
        Buffs*  : gwArray[Buff]
        Effects*: gwArray[Effect]
