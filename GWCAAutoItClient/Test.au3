#include 'GWCAClient.au3'

Global Const $g_Build_s_Profs[2] = [7,1]
Global Const $g_Build_s_Attributes[4][2] = [[3],[20,12],[31,3],[35,12]]
Global Const $g_Build_s_Skills[9] = [8,343,381,2107,952,2233,2101,1018,2187]

; Get Process id of topmost guild wars window
Global $iProc = WinGetProcess('[CLASS:ArenaNet_Dx_Window_Class]')
Global $hProc = _GWCA_OpenMemory($iProc)

;Global $hDLL = _GWCA_InjectDLL($hProc,'..\Release\GWCAServer.dll')
;Sleep(3000)

; Connect to pipe of process.
Global $hPipe = _GWCA_NamedPipeConnect($iProc)

; Send a chat commands to the pipe
Local $player = _GWCA_GetAgentPtr($hPipe,_GWCA_GetPlayerId($hPipe))
;ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $player = ' & $player & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console

Local $hItem = _GWCA_GetItemBySlot($hPipe,1,1)
ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $hItem = ' & $hItem & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console

Local $sName = _GWCA_GetItemName($hPipe,$hItem)
ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $sName = ' & $sName & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console

; Disconnect from pipe
_GWCA_NamedPipeDisconnect($hPipe)

;_GWCA_EjectDLL($hProc,$hDLL)

_GWCA_CloseMemory($hProc)