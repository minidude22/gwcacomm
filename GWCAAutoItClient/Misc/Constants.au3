#include-once

; GWCAComm constants file
; Credits to tecka for these constants

Global Enum $TRANSACT_MERCHANT_BUY  = 0x01, _
			$TRANSACT_COLLECTOR_BUY = 0x02, _
			$TRANSACT_CRAFTER_BUY   = 0x03, _
			$TRANSACT_WEAPONSMITH   = 0x04, _
			$TRANSACT_MERCHANT_SELL = 0x0B, _
			$TRANSACT_TRADER_BUY    = 0x0C, _
			$TRANSACT_TRADER_SELL   = 0x0D, _
			$TRANSACT_UNLOCK_RUNE   = 0x0F

Global Const $SIZE_GW_ARRAY = 16
Global Const $tagGW_ARRAY   =    _
              "PTR   Array;"     _
            & "DWORD Allocated;" _
            & "DWORD Current;"   _
            & "DWORD Default;"

Global Const $QUANTITY_STACK = 250

Global Const $MAX_BLOCKED = 100

Global Enum $RENDERING_OFF, $RENDERING_ON

Global Const $MAX_GOLD_CHARACTER = 0100000
Global Const $MAX_GOLD_STORAGE   = 1000000

Global Enum $FACTION_KURZICK, $FACTION_LUXON

;~ Map Language
Global Enum	$LANGUAGE_OTHER   = 00, _ ;~ English, Korean, Chinese, Japanese, International
			$LANGUAGE_FRENCH  = 02, _
			$LANGUAGE_GERMAN  = 03, _
			$LANGUAGE_ITALIAN = 04, _
			$LANGUAGE_SPANISH = 05, _
			$LANGUAGE_POLISH  = 09, _
			$LANGUAGE_RUSSIAN = 10

;~ Map Region
Global Enum	$REGION_INTERNATIONAL = -02, _
			$REGION_AMERICA       = +00, _
			$REGION_KOREAN        = +01, _
			$REGION_EUROPE        = +02, _
			$REGION_CHINESE       = +03, _
			$REGION_JAPANESE      = +04

;~ District Array For Internal Constants
Global Const $MAP_LANGUAGE_REGION[12][2] = [ _
			[$LANGUAGE_OTHER  , $REGION_AMERICA      ], _
			[$LANGUAGE_OTHER  , $REGION_EUROPE       ], _
			[$LANGUAGE_FRENCH , $REGION_EUROPE       ], _
			[$LANGUAGE_GERMAN , $REGION_EUROPE       ], _
			[$LANGUAGE_ITALIAN, $REGION_EUROPE       ], _
			[$LANGUAGE_SPANISH, $REGION_EUROPE       ], _
			[$LANGUAGE_POLISH , $REGION_EUROPE       ], _
			[$LANGUAGE_RUSSIAN, $REGION_EUROPE       ], _
			[$LANGUAGE_OTHER  , $REGION_INTERNATIONAL], _
			[$LANGUAGE_OTHER  , $REGION_KOREAN       ], _
			[$LANGUAGE_OTHER  , $REGION_CHINESE      ], _
			[$LANGUAGE_OTHER  , $REGION_JAPANESE     ]]

;~ District Constants For Use With _GuildWars_TravelTo
Global Enum	$DISTRICT_RANDOM          = -2, _
			$DISTRICT_CURRENT         = -1, _
			$DISTRICT_AMERICA_ENGLISH = 00, _
			$DISTRICT_EUROPE_ENGLISH  = 01, _
			$DISTRICT_EUROPE_FRENCH   = 02, _
			$DISTRICT_EUROPE_GERMAN   = 03, _
			$DISTRICT_EUROPE_ITALIAN  = 04, _
			$DISTRICT_EUROPE_SPANISH  = 05, _
			$DISTRICT_EUROPE_POLISH   = 06, _
			$DISTRICT_EUROPE_RUSSIAN  = 07, _
			$DISTRICT_ASIA_KOREAN     = 08, _
			$DISTRICT_ASIA_CHINESE    = 09, _
			$DISTRICT_ASIA_JAPANESE   = 10, _
			$DISTRICT_INTERNATIONAL   = 11

Global Enum $MODEL_ATTACK_1 = 0x0060, _
            $MODEL_ATTACK_2 = 0x0440, _
            $MODEL_ATTACK_3 = 0x0060, _
            $MODEL_KNOCKED  = 0x0450

;~ Party Masks
Global Enum $IS_HARDMODE = 10, _
			$IS_DEFEATED = 20

;~ Use key type
Global Enum $CHEST_USE_LOCKPICK = 1, _
			$CHEST_USE_KEY      = 2

;~ Difficulty Mode
Global Enum $MODE_NORMAL = 0, _
			$MODE_HARD   = 1

;~ Agent Effects
Global Enum $EFFECT_BLEEDING    = 0x0001, _
			$EFFECT_CONDITIONED = 0x0002, _
			$EFFECT_DEAD        = 0x0010, _
			$EFFECT_DEEPWOUND   = 0x0020, _
			$EFFECT_POISONED    = 0x0040, _
			$EFFECT_ENCHANTED   = 0x0080, _
			$EFFECT_DEGENHEX    = 0x0400, _
			$EFFECT_HEXED       = 0x0800, _
			$EFFECT_WEAPONSPELL = 0x8000

;~ Agent Type
Global Enum $AGENT_TYPE_LIVING = 0x00DB, _
			$AGENT_TYPE_STATIC = 0x0200, _
			$AGENT_TYPE_ITEM   = 0x0400

;~ Allegiance
Global Enum $ALLEGIANCE_ALLY        = 01, _
			$ALLEGIANCE_CREATURE    = 02, _
			$ALLEGIANCE_ENEMY       = 03, _
			$ALLEGIANCE_PET_SPIRIT  = 04, _
			$ALLEGIANCE_MINION      = 05, _
			$ALLEGIANCE_NPC_MINIPET = 06

;~ AoE Range
Global Enum $RANGE_ADJACENT = 0156, _
			$RANGE_NEARBY   = 0240, _
			$RANGE_AREA     = 0312, _
			$RANGE_EARSHOT  = 1000, _
			$RANGE_SPELL    = 1250, _
			$RANGE_SPIRIT   = 2500, _
			$RANGE_COMPASS  = 5000

;~ Attribute ID
Global Enum $ATTRIBUTE_FAST_CASTING        = 00, _
			$ATTRIBUTE_ILLUSION_MAGIC      = 01, _
			$ATTRIBUTE_DOMINATION_MAGIC    = 02, _
			$ATTRIBUTE_INSPIRATION_MAGIC   = 03, _
			$ATTRIBUTE_BLOOD_MAGIC         = 04, _
			$ATTRIBUTE_DEATH_MAGIC         = 05, _
			$ATTRIBUTE_SOUL_REAPING        = 06, _
			$ATTRIBUTE_CURSES              = 07, _
			$ATTRIBUTE_AIR_MAGIC           = 08, _
			$ATTRIBUTE_EARTH_MAGIC         = 09, _
			$ATTRIBUTE_FIRE_MAGIC          = 10, _
			$ATTRIBUTE_WATER_MAGIC         = 11, _
			$ATTRIBUTE_ENERGY_STORAGE      = 12, _
			$ATTRIBUTE_HEALING_PRAYERS     = 13, _
			$ATTRIBUTE_SMITING_PRAYERS     = 14, _
			$ATTRIBUTE_PROTECTION_PRAYERS  = 15, _
			$ATTRIBUTE_DIVINE_FAVOR        = 16, _
			$ATTRIBUTE_STRENGTH            = 17, _
			$ATTRIBUTE_AXE_MASTERY         = 18, _
			$ATTRIBUTE_HAMMER_MASTERY      = 19, _
			$ATTRIBUTE_SWORDSMANSHIP       = 20, _
			$ATTRIBUTE_TACTICS             = 21, _
			$ATTRIBUTE_BEAST_MASTERY       = 22, _
			$ATTRIBUTE_EXPERTISE           = 23, _
			$ATTRIBUTE_WILDERNESS_SURVIVAL = 24, _
			$ATTRIBUTE_MARKSMANSHIP        = 25, _
			$ATTRIBUTE_DAGGER_MASTERY      = 29, _
			$ATTRIBUTE_DEADLY_ARTS         = 30, _
			$ATTRIBUTE_SHADOW_ARTS         = 31, _
			$ATTRIBUTE_COMMUNING           = 32, _
			$ATTRIBUTE_RESTORATION_MAGIC   = 33, _
			$ATTRIBUTE_CHANNELING_MAGIC    = 34, _
			$ATTRIBUTE_CRITICAL_STRIKES    = 35, _
			$ATTRIBUTE_SPAWNING_POWER      = 36, _
			$ATTRIBUTE_SPEAR_MASTERY       = 37, _
			$ATTRIBUTE_COMMAND             = 38, _
			$ATTRIBUTE_MOTIVATION          = 39, _
			$ATTRIBUTE_LEADERSHIP          = 40, _
			$ATTRIBUTE_SCYTHE_MASTERY      = 41, _
			$ATTRIBUTE_WIND_PRAYERS        = 42, _
			$ATTRIBUTE_EARTH_PRAYERS       = 43, _
			$ATTRIBUTE_MYSTICISM           = 44

;~ Bag ID
Global Enum $BAG_BACKPACK         = 01, _
			$BAG_BELTPOUCH        = 02, _
			$BAG_BAG_1            = 03, _
			$BAG_BAG_2            = 04, _
			$BAG_EQUIPMENT_PACK   = 05, _
			$BAG_MATERIAL_STORAGE = 06, _
			$BAG_UNCLAIMED_ITEMS  = 07, _
			$BAG_STORAGE_1        = 08, _
			$BAG_STORAGE_2        = 09, _
			$BAG_STORAGE_3        = 10, _
			$BAG_STORAGE_4        = 11, _
			$BAG_STORAGE_5        = 12, _
			$BAG_STORAGE_6        = 13, _
			$BAG_STORAGE_7        = 14, _
			$BAG_STORAGE_8        = 15, _
			$BAG_STORAGE_9        = 16, _
			$BAG_EQUIPPED_ITEMS   = 17

Global Const $INVENTORY_BAGS[5]  = [4, $BAG_BACKPACK, $BAG_BELTPOUCH, $BAG_BAG_1, $BAG_BAG_2]
Global Const $STORAGE_BAGS[10]   = [9, $BAG_STORAGE_1, $BAG_STORAGE_2, $BAG_STORAGE_3, $BAG_STORAGE_4, $BAG_STORAGE_5, $BAG_STORAGE_6, $BAG_STORAGE_7, $BAG_STORAGE_8, $BAG_STORAGE_9]
Global Const $USABLE_BAGS[14]    = [13, $BAG_BACKPACK, $BAG_BELTPOUCH, $BAG_BAG_1, $BAG_BAG_2, $BAG_STORAGE_1, $BAG_STORAGE_2, $BAG_STORAGE_3, $BAG_STORAGE_4, $BAG_STORAGE_5, $BAG_STORAGE_6, $BAG_STORAGE_7, $BAG_STORAGE_8, $BAG_STORAGE_9]

;~ Combo ID
Global Enum $COMBO_LEAD_ATTACK    = 01, _
			$COMBO_OFFHAND_ATTACK = 02, _
			$COMBO_DUAL_ATTACK    = 03

;~ Combo Requirement
Global Enum $COMBO_REQUIREMENT_FOLLOWS_DUAL    = 01, _
			$COMBO_REQUIREMENT_FOLLOWS_LEAD    = 02, _
			$COMBO_REQUIREMENT_FOLLOWS_OFFHAND = 03

;~ Extra ID
Global Enum $DYE_BLUE   = 02, _
			$DYE_GREEN  = 03, _
			$DYE_PURPLE = 04, _
			$DYE_RED    = 05, _
			$DYE_YELLOW = 06, _
			$DYE_BROWN  = 07, _
			$DYE_ORANGE = 08, _
			$DYE_SILVER = 09, _
			$DYE_BLACK  = 10, _
			$DYE_GRAY   = 11, _
			$DYE_WHITE  = 12, _
			$DYE_PINK   = 13

;~ Hero ID
Global Enum $HERO_NORGU              = 01, _
			$HERO_GOREN              = 02, _
			$HERO_TAHLKORA           = 03, _
			$HERO_MASTER_OF_WHISPERS = 04, _
			$HERO_ACOLYTE_JIN        = 05, _
			$HERO_KOSS               = 06, _
			$HERO_DUNKORO            = 07, _
			$HERO_ACOLYTE_SOUSUKE    = 08, _
			$HERO_MELONNI            = 09, _
			$HERO_ZHED_SHADOWHOOF    = 10, _
			$HERO_GENERAL_MORGAHN    = 11, _
			$HERO_MAGRID_THE_SLY     = 12, _
			$HERO_ZENMAI             = 13, _
			$HERO_OLIAS              = 14, _
			$HERO_RAZAH              = 15, _
			$HERO_MOX                = 16, _
			$HERO_KEIRAN_THACKERAY   = 17, _
			$HERO_JORA               = 18, _
			$HERO_PYRE_FIERCESHOT    = 19, _
			$HERO_ANTON              = 20, _
			$HERO_LIVIA              = 21, _
			$HERO_HAYDA              = 22, _
			$HERO_KAHMU              = 23, _
			$HERO_GWEN               = 24, _
			$HERO_XANDRA             = 25, _
			$HERO_VEKK               = 26, _
			$HERO_OGDEN_STONEHEALER  = 27, _
			$HERO_MERCENARY_1        = 28, _
			$HERO_MERCENARY_2        = 29, _
			$HERO_MERCENARY_3        = 30, _
			$HERO_MERCENARY_4        = 31, _
			$HERO_MERCENARY_5        = 32, _
			$HERO_MERCENARY_6        = 33, _
			$HERO_MERCENARY_7        = 34, _
			$HERO_MERCENARY_8        = 35, _
			$HERO_MIKU               = 36, _
			$HERO_ZEI_RI             = 37

;~ Item Type
Global Enum $TYPE_SALVAGE            = 00, _
			$TYPE_AXE                = 02, _
			$TYPE_BAG                = 03, _
			$TYPE_BOOTS              = 04, _
			$TYPE_BOW                = 05, _
			$TYPE_CHESTPIECE         = 07, _
			$TYPE_RUNEMOD            = 08, _
			$TYPE_USABLE             = 09, _
			$TYPE_DYE                = 10, _
			$TYPE_MATERIALS_ZCOINS   = 11, _
			$TYPE_OFFHAND            = 12, _
			$TYPE_GLOVES             = 13, _
			$TYPE_HAMMER             = 15, _
			$TYPE_HEADPIECE          = 16, _
			$TYPE_CC_SHARDS          = 17, _
			$TYPE_KEY                = 18, _
			$TYPE_LEGGINGS           = 19, _
			$TYPE_GOLD_COINS         = 20, _
			$TYPE_QUEST_ITEM         = 21, _
			$TYPE_WAND               = 22, _
			$TYPE_SHIELD             = 24, _
			$TYPE_STAFF              = 26, _
			$TYPE_SWORD              = 27, _
			$TYPE_KIT                = 29, _
			$TYPE_TROPHY             = 30, _
			$TYPE_SCROLL             = 31, _
			$TYPE_DAGGERS            = 32, _
			$TYPE_MINIPET            = 34, _
			$TYPE_SCYTHE             = 35, _
			$TYPE_SPEAR              = 36, _
			$TYPE_COSTUME_HEADPIECE  = 45

;~ Profession
Global Enum $PROFESSION_NONE         = 00, _
			$PROFESSION_WARRIOR      = 01, _
			$PROFESSION_RANGER       = 02, _
			$PROFESSION_MONK         = 03, _
			$PROFESSION_NECROMANCER  = 04, _
			$PROFESSION_MESMER       = 05, _
			$PROFESSION_ELEMENTALIST = 06, _
			$PROFESSION_ASSASSIN     = 07, _
			$PROFESSION_RITUALIST    = 08, _
			$PROFESSION_PARAGON      = 09, _
			$PROFESSION_DERVISH      = 10

;~ Rarity
Global Enum $RARITY_WHITE    = 002621, _
			$RARITY_BLUE     = 002623, _
			$RARITY_PURPLE   = 002626, _
			$RARITY_GOLD     = 002624, _
			$RARITY_GREEN    = 002627, _
			$RARITY_RED      = 033026

;~ Weapon type
Global Enum $WEAPON_BOW       = 01, _
			$WEAPON_AXE       = 02, _
			$WEAPON_HAMMER    = 03, _
			$WEAPON_DAGGERS   = 04, _
			$WEAPON_SCYTHE    = 05, _
			$WEAPON_SPEAR     = 06, _
			$WEAPON_SWORD     = 07, _
			$WEAPON_CHAOS     = 08, _
			$WEAPON_COLD      = 09, _
			$WEAPON_DARK      = 10, _
			$WEAPON_EARTH     = 11, _
			$WEAPON_LIGHTNING = 12, _
			$WEAPON_FIRE      = 13, _
			$WEAPON_HOLY      = 14

;~ Online Status
Global Enum $STATUS_OFFLINE        = 00, _
			$STATUS_ONLINE         = 01, _
			$STATUS_DO_NOT_DISTURB = 02, _
			$STATUS_AWAY           = 03

;~ Map Loading
Global Enum $MAP_OUTPOST    = 00, _
			$MAP_EXPLORABLE = 01, _
			$MAP_LOADING    = 02

;~ Skill Condition
Global Enum $SKILL_CONDITION_BLEEDING   = 0x0001, _
			$SKILL_CONDITION_BURNING    = 0x0004, _
			$SKILL_CONDITION_CRIPPLE    = 0x0008, _
			$SKILL_CONDITION_DEEP_WOUND = 0x0010, _
			$SKILL_CONDITION_EARTH_HEX  = 0x0040, _
			$SKILL_CONDITION_KNOCK_DOWN = 0x0080, _
			$SKILL_CONDITION_WEAKNESS   = 0x0400, _
			$SKILL_CONDITION_WATER_HEX  = 0x0800

;~ Skill Effect1
Global Enum $SKILL_EFFECT1_BLEEDING           = 0x0001, _
			$SKILL_EFFECT1_BLIND              = 0x0002, _
			$SKILL_EFFECT1_BURNING            = 0x0004, _
			$SKILL_EFFECT1_CRIPPLE            = 0x0008, _
			$SKILL_EFFECT1_DEEP_WOUND         = 0x0010, _
			$SKILL_EFFECT1_DISEASE_CHILBLAINS = 0x0020, _
			$SKILL_EFFECT1_EARTH_SNARE_HEX    = 0x0040, _
			$SKILL_EFFECT1_KNOCKDOWN          = 0x0080, _
			$SKILL_EFFECT1_POISON             = 0x0100, _
			$SKILL_EFFECT1_DAZE               = 0x0200, _
			$SKILL_EFFECT1_WEAKNESS           = 0x0400, _
			$SKILL_EFFECT1_WATER_HEX          = 0x0800, _
			$SKILL_EFFECT1_HEAL               = 0x1000

;~ Skill Effect2
Global Enum $SKILL_EFFECT2_INTERRUPT_ALIKE            = 0x000001, _
			$SKILL_EFFECT2_HEALS1                     = 0x000002, _
			$SKILL_EFFECT2_HEALS2                     = 0x000004, _
			$SKILL_EFFECT2_RESURRECTION               = 0x000008, _
			$SKILL_EFFECT2_HEALTH_SACRIFICE           = 0x000020, _
			$SKILL_EFFECT2_ENERGY_STEAL               = 0x000040, _
			$SKILL_EFFECT2_BLOCKING                   = 0x000080, _
			$SKILL_EFFECT2_ENERGY_GAIN_COST_REDUCTION = 0x000100, _
			$SKILL_EFFECT2_TOUCH_PBAOE                = 0x000200, _
			$SKILL_EFFECT2_HEX_REMOVAL                = 0x000800, _
			$SKILL_EFFECT2_CONDITION_REMOVAL          = 0x001000, _
			$SKILL_EFFECT2_REMOVE_ENCHANTMENT_SELF    = 0x002000, _
			$SKILL_EFFECT2_TRIGGERS_ON_ATTACK         = 0x004000, _
			$SKILL_EFFECT2_IAS_IMS                    = 0x008000, _
			$SKILL_EFFECT2_KNOCK_DOWN_SELF            = 0x010000

;~ Skill Equip Type
Global Enum $SKILL_EQUIPTYPE_MONSTER_SKILLS = 0x00, _
			$SKILL_EQUIPTYPE_PLAYER_SKILLS  = 0x01, _
			$SKILL_EQUIPTYPE_EVENT_SKILLS   = 0x02, _
			$SKILL_EQUIPTYPE_NPC_SKILLS     = 0x04

;~ Skill Special
Global Enum $SKILL_SPECIAL_EXHAUSTION                   = 0x00000001, _
			$SKILL_SPECIAL_TOUCH_SKILL                  = 0x00000002, _
			$SKILL_SPECIAL_ELITE                        = 0x00000004, _
			$SKILL_SPECIAL_HALF_RANGE                   = 0x00000008, _
			$SKILL_SPECIAL_EASILY_INTERRUPTED           = 0x00000020, _
			$SKILL_SPECIAL_CHANCE_TO_FAIL               = 0x00000040, _
			$SKILL_SPECIAL_ARMOR_DECREASE_ON_ACTIVATION = 0x00000080, _
			$SKILL_SPECIAL_SPIRIT_TRAP_PREPARATION      = 0x00000100, _
			$SKILL_SPECIAL_DEGEN_HEX                    = 0x00000200, _
			$SKILL_SPECIAL_MONSTER_SIEGE                = 0x00000400, _
			$SKILL_SPECIAL_RESURRECTION                 = 0x00000800, _
			$SKILL_SPECIAL_CONDITIONS                   = 0x00002000, _
			$SKILL_SPECIAL_MONSTER_SPIRIT               = 0x00004000, _
			$SKILL_SPECIAL_DUAL_ATTACK                  = 0x00008000, _
			$SKILL_SPECIAL_CORPSE_EXPLOITATION          = 0x00040000, _
			$SKILL_SPECIAL_PVE_ONLY                     = 0x00080000, _
			$SKILL_SPECIAL_SCROLL_ENVIRONMENTAL_EFFECT  = 0x00100000, _
			$SKILL_SPECIAL_PVP_VERSION                  = 0x00400000, _
			$SKILL_SPECIAL_FLASH_ENCHANTMENT            = 0x00800000

;~ Skill Type
Global Enum $SKILL_TYPE_BOUNTY             = 01, _
			$SKILL_TYPE_SCROLL             = 02, _
			$SKILL_TYPE_STANCE             = 03, _
			$SKILL_TYPE_HEX                = 04, _
			$SKILL_TYPE_SPELL              = 05, _
			$SKILL_TYPE_ENCHANTMENT        = 06, _
			$SKILL_TYPE_SIGNET             = 07, _
			$SKILL_TYPE_CONDITION          = 08, _
			$SKILL_TYPE_WELL               = 09, _
			$SKILL_TYPE_SKILL_A            = 10, _
			$SKILL_TYPE_WARD               = 11, _
			$SKILL_TYPE_GLYPH              = 12, _
			$SKILL_TYPE_TITLE              = 13, _
			$SKILL_TYPE_ATTACK             = 14, _
			$SKILL_TYPE_SHOUT              = 15, _
			$SKILL_TYPE_SKILL_B            = 16, _
			$SKILL_TYPE_PASSIVE            = 17, _
			$SKILL_TYPE_ENVIRONMENTAL      = 18, _
			$SKILL_TYPE_PREPARATION        = 19, _
			$SKILL_TYPE_PET_ATTACK         = 20, _
			$SKILL_TYPE_TRAP               = 21, _
			$SKILL_TYPE_RITUAL             = 22, _
			$SKILL_TYPE_ENVIRONMENTAL_TRAP = 23, _
			$SKILL_TYPE_ITEM_SPELL         = 24, _
			$SKILL_TYPE_WEAPON_SPELL       = 25, _
			$SKILL_TYPE_FORM               = 26, _
			$SKILL_TYPE_CHANT              = 27, _
			$SKILL_TYPE_ECHO_REFRAIN       = 28, _
			$SKILL_TYPE_DISGUISE           = 29

;~ Skill Weapon Requirement
Global Enum $SKILL_WEAPON_REQUIREMENT_AXE     = 0x0001, _
			$SKILL_WEAPON_REQUIREMENT_BOW     = 0x0002, _
			$SKILL_WEAPON_REQUIREMENT_DAGGERS = 0x0008, _
			$SKILL_WEAPON_REQUIREMENT_HAMMER  = 0x0010, _
			$SKILL_WEAPON_REQUIREMENT_SCYTHE  = 0x0020, _
			$SKILL_WEAPON_REQUIREMENT_SPEAR   = 0x0040, _
			$SKILL_WEAPON_REQUIREMENT_SWORD   = 0x0080

;~ Skill Target
Global Enum $SKILL_TARGET_SELF_NONE  = 00, _
			$SKILL_TARGET_CONDITIONS = 01, _
			$SKILL_TARGET_ALLY       = 03, _
			$SKILL_TARGET_OTHER_ALLY = 04, _
			$SKILL_TARGET_ENEMY      = 05, _
			$SKILL_TARGET_DEAD_ALLY  = 06, _
			$SKILL_TARGET_MINION     = 14, _
			$SKILL_TARGET_GROUND     = 16

;~ Team
Global Enum $TEAM_GRAY   = 00, _
			$TEAM_BLUE   = 01, _
			$TEAM_RED    = 02, _
			$TEAM_YELLOW = 03, _
			$TEAM_PURPLE = 04, _
			$TEAM_CYAN   = 05

;~ TypeMap
Global Enum $TYPEMAP_IN_STANCE             = 0x00000001, _
			$TYPEMAP_HAS_QUEST             = 0x00000002, _
			$TYPEMAP_DEAD                  = 0x00000008, _
			$TYPEMAP_FEMALE                = 0x00000200, _
			$TYPEMAP_BOSS_GLOW             = 0x00000400, _
			$TYPEMAP_HIDING_CAPE           = 0x00001000, _
			$TYPEMAP_PARTY_WINDOW_VIEWABLE = 0x00020000, _
			$TYPEMAP_SPAWNED_AGENT         = 0x00040000, _
			$TYPEMAP_SELF_OBSERVED         = 0x00400000

;~ Interaction (Many of these are outdated)
Global Enum $IDENTIFIED           = 0x00000001, _
			$ARMOR_PLAYER_MONSTER = 0x00000002, _
			$ARMOR_PLAYER         = 0x00000004, _
			$GREEN                = 0x00000010, _
			$COMMON_MATERIALS     = 0x00000020, _
			$DROPS_FROM_MONSTER   = 0x00000040, _
			$UNTRADABLE           = 0x00000100, _
			$UNDYEABLE            = 0x00000200, _
			$FIXED_PREFIX         = 0x00004000, _
			$FIXED_SUFFIX         = 0x00008000, _
			$HAS_PREFIX           = 0x00010000, _
			$GOLD                 = 0x00020000, _
			$UNCUSTOMIZED         = 0x00080000, _
			$HAS_SUFFIX           = 0x00100000, _
			$TWO_HANDED           = 0x00200000, _
			$PURPLE               = 0x00400000, _
			$UNIDENTIFIED         = 0x00800000, _
			$USABLE               = 0x01000000, _
			$MAIN_HAND            = 0x02000000, _
			$HAS_INSCRIPTION      = 0x04000000, _
			$INSCRIBABLE          = 0x08000000, _
			$BONUS_WEAPON         = 0x10000000, _
			$ALWAYS_SET           = 0x02000000

			Global Enum $MATERIAL_BONE                    = 921, _
			$MATERIAL_IRON_INGOT              = 948, _
			$MATERIAL_TANNED_HIDE_SQUARE      = 940, _
			$MATERIAL_SCALE                   = 953, _
			$MATERIAL_CHITIN_FRAGMENT         = 954, _
			$MATERIAL_BOLT_OF_CLOTH           = 925, _
			$MATERIAL_WOOD_PLANK              = 946, _
			$MATERIAL_GRANITE_SLAB            = 955, _
			$MATERIAL_PILE_OF_GLITTERING_DUST = 929, _
			$MATERIAL_PLANT_FIBER             = 934, _
			$MATERIAL_FEATHER                 = 933

Global Enum $RARE_MATERIAL_FUR_SQUARE             = 0941, _
			$RARE_MATERIAL_BOLT_OF_LINEN          = 0926, _
			$RARE_MATERIAL_BOLT_OF_DAMASK         = 0927, _
			$RARE_MATERIAL_BOLT_OF_SILK           = 0928, _
			$RARE_MATERIAL_GLOB_OF_ECTOPLASM      = 0930, _
			$RARE_MATERIAL_STEEL_INGOT            = 0949, _
			$RARE_MATERIAL_DELDRIMOR_STEEL_INGOT  = 0950, _
			$RARE_MATERIAL_MONSTROUS_CLAW         = 0923, _
			$RARE_MATERIAL_MONSTROUS_EYE          = 0931, _
			$RARE_MATERIAL_MONSTROUS_FANG         = 0932, _
			$RARE_MATERIAL_RUBY                   = 0937, _
			$RARE_MATERIAL_SAPPHIRE               = 0938, _
			$RARE_MATERIAL_DIAMOND                = 0935, _
			$RARE_MATERIAL_ONYX_GEMSTONE          = 0936, _
			$RARE_MATERIAL_LUMP_OF_CHARCOAL       = 0922, _
			$RARE_MATERIAL_OBSIDIAN_SHARD         = 0945, _
			$RARE_MATERIAL_TEMPERED_GLASS_VIAL    = 0939, _
			$RARE_MATERIAL_LEATHER_SQUARE         = 0942, _
			$RARE_MATERIAL_ELONIAN_LEATHER_SQUARE = 0943, _
			$RARE_MATERIAL_VIAL_OF_INK            = 0944, _
			$RARE_MATERIAL_ROLL_OF_PARCHMENT      = 0951, _
			$RARE_MATERIAL_ROLL_OF_VELLUM         = 0952, _
			$RARE_MATERIAL_SPIRITWOOD_PLANK       = 0956, _
			$RARE_MATERIAL_AMBER_CHUNK            = 6532, _
			$RARE_MATERIAL_JADEITE_SHARD          = 6533

Global Enum $MODID_KIT_USES = 0x25E8

Global Enum $SALVAGE_KIT                 = 02992, _
			$EXPERT_SALVAGE_KIT          = 02991, _
			$SUPERIOR_SALVAGE_KIT        = 05900, _
            $PERFECT_SALVAGE_KIT         = 25881, _
			$IDENTIFICATION_KIT          = 02989, _
			$SUPERIOR_IDENTIFICATION_KIT = 05899

Global Const $VIAL_OF_DYE = 146

Global Const $TOME_ELITE_MESMER       = 21787
Global Const $TOME_ELITE_ASSASSIN     = 21786
Global Const $TOME_ELITE_RANGER       = 21792
Global Const $TOME_ELITE_WARRIOR      = 21791
Global Const $TOME_ELITE_MONK         = 21790
Global Const $TOME_ELITE_NECROMANCER  = 21788
Global Const $TOME_ELITE_ELEMENTALIST = 21789
Global Const $TOME_ELITE_DERVISH      = 21793
Global Const $TOME_ELITE_RITUALIST    = 21794
Global Const $TOME_ELITE_PARAGON      = 21795

Global Const $TOME_MESMER       = 21797
Global Const $TOME_ASSASSIN     = 21796
Global Const $TOME_RANGER       = 21802
Global Const $TOME_WARRIOR      = 21801
Global Const $TOME_MONK         = 21800
Global Const $TOME_NECROMANCER  = 21798
Global Const $TOME_ELEMENTALIST = 21799
Global Const $TOME_DERVISH      = 21803
Global Const $TOME_RITUALIST    = 21804
Global Const $TOME_PARAGON      = 21805

Global Const $FLASK_OF_FIREWATER = 2513
Global Const $BLESSING_OF_WAR    = 37843
Global Const $CREME_BRULEE       = 15528
