#include-once

; GWCAComm _Skill_ Handlers
; Auto Generated


Func _Skill_ID($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0000, "long*", 0x04)
EndFunc ; <== ID

Func _Skill_Unknown1($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0004, "dword*", 0x04)
EndFunc ; <== Unknown1

Func _Skill_campaign($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0008, "long*", 0x04)
EndFunc ; <== campaign

Func _Skill_Type($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x000C, "long*", 0x04)
EndFunc ; <== Type

Func _Skill_Special($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0010, "long*", 0x04)
EndFunc ; <== Special

Func _Skill_ComboReq($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0014, "long*", 0x04)
EndFunc ; <== ComboReq

Func _Skill_Effect1($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0018, "long*", 0x04)
EndFunc ; <== Effect1

Func _Skill_Condition($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x001C, "long*", 0x04)
EndFunc ; <== Condition

Func _Skill_Effect2($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0020, "long*", 0x04)
EndFunc ; <== Effect2

Func _Skill_WeaponReq($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0024, "long*", 0x04)
EndFunc ; <== WeaponReq

Func _Skill_Profession($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0028, "byte*", 0x01)
EndFunc ; <== Profession

Func _Skill_Attribute($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0029, "byte*", 0x01)
EndFunc ; <== Attribute

Func _Skill_PvESkill($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x002A, "short*", 0x02)
EndFunc ; <== PvESkill

Func _Skill_PvPID($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x002C, "long*", 0x04)
EndFunc ; <== PvPID

Func _Skill_Combo($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0030, "byte*", 0x01)
EndFunc ; <== Combo

Func _Skill_Target($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0031, "byte*", 0x01)
EndFunc ; <== Target

Func _Skill_unknown3($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0032, "byte*", 0x01)
EndFunc ; <== unknown3

Func _Skill_EquipType($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0033, "byte*", 0x01)
EndFunc ; <== EquipType

Func _Skill_Unknown4a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0034, "byte*", 0x01)
EndFunc ; <== Unknown4a

Func _Skill_EnergyCost($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0035, "byte*", 0x01)
EndFunc ; <== EnergyCost

Func _Skill_HealthCost($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0036, "short*", 0x02)
EndFunc ; <== HealthCost

Func _Skill_Adrenaline($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0038, "dword*", 0x04)
EndFunc ; <== Adrenaline

Func _Skill_Activation($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x003C, "float*", 0x04)
EndFunc ; <== Activation

Func _Skill_Aftercast($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0040, "float*", 0x04)
EndFunc ; <== Aftercast

Func _Skill_Duration0($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0044, "long*", 0x04)
EndFunc ; <== Duration0

Func _Skill_Duration15($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0048, "long*", 0x04)
EndFunc ; <== Duration15

Func _Skill_Recharge($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x004C, "long*", 0x04)
EndFunc ; <== Recharge

Func _Skill_Unknown5a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0050, "dword*", 0x04)
EndFunc ; <== Unknown5a

Func _Skill_Unknown5b($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0054, "dword*", 0x04)
EndFunc ; <== Unknown5b

Func _Skill_Unknown5c($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0058, "dword*", 0x04)
EndFunc ; <== Unknown5c

Func _Skill_Scale0($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x005C, "long*", 0x04)
EndFunc ; <== Scale0

Func _Skill_Scale15($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0060, "long*", 0x04)
EndFunc ; <== Scale15

Func _Skill_BonusScale0($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0064, "long*", 0x04)
EndFunc ; <== BonusScale0

Func _Skill_BonusScale15($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0068, "long*", 0x04)
EndFunc ; <== BonusScale15

Func _Skill_AoERange($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x006C, "float*", 0x04)
EndFunc ; <== AoERange

Func _Skill_ConstEffect($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0070, "dword*", 0x04)
EndFunc ; <== ConstEffect

Func _Skill_unknown6a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0074, "dword*", 0x04)
EndFunc ; <== unknown6a

Func _Skill_unknown7a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0078, "dword*", 0x04)
EndFunc ; <== unknown7a

Func _Skill_unknown8a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x007C, "dword*", 0x04)
EndFunc ; <== unknown8a

Func _Skill_unknown9a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0080, "dword*", 0x04)
EndFunc ; <== unknown9a

Func _Skill_unknown10a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0084, "dword*", 0x04)
EndFunc ; <== unknown10a

Func _Skill_unknown11a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0088, "dword*", 0x04)
EndFunc ; <== unknown11a

Func _Skill_unknown12a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x008C, "dword*", 0x04)
EndFunc ; <== unknown12a

Func _Skill_unknown14a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0094, "dword*", 0x04)
EndFunc ; <== unknown14a

Func _Skill_tooltip($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x0094, "uint64*", 0x08)
EndFunc ; <== tooltip

Func _Skill_unknown16a($hProcess, $hSkill)
	Return _GWCA_Read($hProcess, $hSkill + 0x009C, "dword*", 0x04)
EndFunc ; <== unknown16a
