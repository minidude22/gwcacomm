#include-once

; GWCAComm _Bag_ Handlers
; Auto Generated


Func _Bag_BagType($hProcess, $hBag)
	Return _GWCA_Read($hProcess, $hBag + 0x0000, "long*", 0x04)
EndFunc ; <== BagType

Func _Bag_index($hProcess, $hBag)
	Return _GWCA_Read($hProcess, $hBag + 0x0004, "long*", 0x04)
EndFunc ; <== index

Func _Bag_id($hProcess, $hBag)
	Return _GWCA_Read($hProcess, $hBag + 0x0008, "long*", 0x04)
EndFunc ; <== id

Func _Bag_containerItem($hProcess, $hBag)
	Return _GWCA_Read($hProcess, $hBag + 0x000C, "ptr*", 0x04)
EndFunc ; <== containerItem

Func _Bag_ItemsCount($hProcess, $hBag)
	Return _GWCA_Read($hProcess, $hBag + 0x0010, "long*", 0x04)
EndFunc ; <== ItemsCount

Func _Bag_bagArray($hProcess, $hBag)
	Return _GWCA_Read($hProcess, $hBag + 0x0014, "ptr*", 0x04)
EndFunc ; <== bagArray

Func _Bag_itemArray($hProcess, $hBag)
	Return _GWCA_Read($hProcess, $hBag + 0x0018, "ptr*", 0x04)
EndFunc ; <== itemArray

Func _Bag_fakeSlots($hProcess, $hBag)
	Return _GWCA_Read($hProcess, $hBag + 0x001C, "long*", 0x04)
EndFunc ; <== fakeSlots

Func _Bag_slots($hProcess, $hBag)
	Return _GWCA_Read($hProcess, $hBag + 0x0020, "long*", 0x04)
EndFunc ; <== slots
