#include-once

; GWCAComm _Agent_ Handlers
; Auto Generated


Func _Agent_vtable($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0000, "ptr*", 0x04)
EndFunc ; <== vtable

Func _Agent_unknown1a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0004, "dword*", 0x04)
EndFunc ; <== unknown1a

Func _Agent_unknown1b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0008, "ptr*", 0x04)
EndFunc ; <== unknown1b

Func _Agent_unknown1c($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x000C, "ptr*", 0x04)
EndFunc ; <== unknown1c

Func _Agent_unknown1d($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0010, "dword*", 0x04)
EndFunc ; <== unknown1d

Func _Agent_Timer1($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0014, "long*", 0x04)
EndFunc ; <== Timer1

Func _Agent_unknown2a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0018, "dword*", 0x04)
EndFunc ; <== unknown2a

Func _Agent_NearestAgentPtr($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x001C, "ptr*", 0x04)
EndFunc ; <== NearestAgentPtr

Func _Agent_NextAgent($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0020, "ptr*", 0x04)
EndFunc ; <== NextAgent

Func _Agent_unknown3($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0024, "ptr*", 0x04)
EndFunc ; <== unknown3

Func _Agent_unknown3b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0028, "ptr*", 0x04)
EndFunc ; <== unknown3b

Func _Agent_Id($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x002C, "long*", 0x04)
EndFunc ; <== Id

Func _Agent_Z($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0030, "float*", 0x04)
EndFunc ; <== Z

Func _Agent_unknown4a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0034, "dword*", 0x04)
EndFunc ; <== unknown4a

Func _Agent_unknown4b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0038, "dword*", 0x04)
EndFunc ; <== unknown4b

Func _Agent_BoxHoverWidth($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x003C, "float*", 0x04)
EndFunc ; <== BoxHoverWidth

Func _Agent_BoxHoverHeight($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0040, "float*", 0x04)
EndFunc ; <== BoxHoverHeight

Func _Agent_unknown5a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0044, "dword*", 0x04)
EndFunc ; <== unknown5a

Func _Agent_unknown5b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0048, "dword*", 0x04)
EndFunc ; <== unknown5b

Func _Agent_Rotation($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x004C, "float*", 0x04)
EndFunc ; <== Rotation

Func _Agent_unknown6a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0050, "float*", 0x04)
EndFunc ; <== unknown6a

Func _Agent_unknown6b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0054, "float*", 0x04)
EndFunc ; <== unknown6b

Func _Agent_NameProperties($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0058, "dword*", 0x04)
EndFunc ; <== NameProperties

Func _Agent_unknown7a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x005C, "dword*", 0x04)
EndFunc ; <== unknown7a

Func _Agent_unknown7b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0060, "dword*", 0x04)
EndFunc ; <== unknown7b

Func _Agent_unknown7c($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0064, "dword*", 0x04)
EndFunc ; <== unknown7c

Func _Agent_unknown7d($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0068, "dword*", 0x04)
EndFunc ; <== unknown7d

Func _Agent_unknown7e($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x006C, "dword*", 0x04)
EndFunc ; <== unknown7e

Func _Agent_unknown7f($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0070, "word*", 0x02)
EndFunc ; <== unknown7f

Func _Agent_unknown7g($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0072, "word*", 0x02)
EndFunc ; <== unknown7g

Func _Agent_X($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0074, "float*", 0x04)
EndFunc ; <== X

Func _Agent_Y($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0078, "float*", 0x04)
EndFunc ; <== Y

Func _Agent_Ground($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x007C, "long*", 0x04)
EndFunc ; <== Ground

Func _Agent_unknown8b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0080, "dword*", 0x04)
EndFunc ; <== unknown8b

Func _Agent_NameTagX($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0084, "float*", 0x04)
EndFunc ; <== NameTagX

Func _Agent_NameTagY($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0088, "float*", 0x04)
EndFunc ; <== NameTagY

Func _Agent_NameTagZ($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x008C, "float*", 0x04)
EndFunc ; <== NameTagZ

Func _Agent_unknown9a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0090, "word*", 0x02)
EndFunc ; <== unknown9a

Func _Agent_unknown9b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0092, "word*", 0x02)
EndFunc ; <== unknown9b

Func _Agent_unknown9c($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0094, "float*", 0x04)
EndFunc ; <== unknown9c

Func _Agent_unknown9d($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0098, "dword*", 0x04)
EndFunc ; <== unknown9d

Func _Agent_Type($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x009C, "long*", 0x04)
EndFunc ; <== Type

Func _Agent_MoveX($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00A0, "float*", 0x04)
EndFunc ; <== MoveX

Func _Agent_MoveY($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00A4, "float*", 0x04)
EndFunc ; <== MoveY

Func _Agent_unknown10a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00A8, "float*", 0x04)
EndFunc ; <== unknown10a

Func _Agent_unknown10b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00AC, "float*", 0x04)
EndFunc ; <== unknown10b

Func _Agent_unknown10c($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00B0, "float*", 0x04)
EndFunc ; <== unknown10c

Func _Agent_unknown10d($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00B4, "float*", 0x04)
EndFunc ; <== unknown10d

Func _Agent_unknown10e($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00B8, "float*", 0x04)
EndFunc ; <== unknown10e

Func _Agent_unknown10f($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00BC, "float*", 0x04)
EndFunc ; <== unknown10f

Func _Agent_unknown10g($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00C0, "float*", 0x04)
EndFunc ; <== unknown10g

Func _Agent_Owner($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00C4, "long*", 0x04)
EndFunc ; <== Owner

Func _Agent_ItemID($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00C8, "long*", 0x04)
EndFunc ; <== ItemID

Func _Agent_unknown30b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00CC, "dword*", 0x04)
EndFunc ; <== unknown30b

Func _Agent_ExtraType($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00D0, "long*", 0x04)
EndFunc ; <== ExtraType

Func _Agent_unknown11a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00D4, "ptr*", 0x04)
EndFunc ; <== unknown11a

Func _Agent_unknown11b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00D8, "ptr*", 0x04)
EndFunc ; <== unknown11b

Func _Agent_Animation($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00DC, "dword*", 0x01)
EndFunc ; <== Animation

Func _Agent_unknown11d($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00E0, "float*", 0x04)
EndFunc ; <== unknown11d

Func _Agent_unknown11e($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00E4, "dword*", 0x04)
EndFunc ; <== unknown11e

Func _Agent_unknown11f($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00E8, "dword*", 0x04)
EndFunc ; <== unknown11f

Func _Agent_AttackSpeed($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00EC, "float*", 0x04)
EndFunc ; <== AttackSpeed

Func _Agent_AttackSpeedModifier($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00F0, "float*", 0x04)
EndFunc ; <== AttackSpeedModifier

Func _Agent_PlayerNumber($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00F4, "word*", 0x02)
EndFunc ; <== PlayerNumber

Func _Agent_PlayerNumberArray($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00F4, "dword*", 0x04)
EndFunc ; <== PlayerNumberArray

Func _Agent_unknown12b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00F8, "dword*", 0x06)
EndFunc ; <== unknown12b

Func _Agent_Equip($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x00FC, "ptr*", 0x04)
EndFunc ; <== Equip

Func _Agent_unknown13a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0100, "dword*", 0x04)
EndFunc ; <== unknown13a

Func _Agent_unknown13b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0104, "dword*", 0x04)
EndFunc ; <== unknown13b

Func _Agent_unknown13c(guild)($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0108, "word*", 0x02)
EndFunc ; <== unknown13c(guild)

Func _Agent_Primary($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x010A, "byte*", 0x01)
EndFunc ; <== Primary

Func _Agent_Secondary($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x010B, "byte*", 0x01)
EndFunc ; <== Secondary

Func _Agent_Level($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x010C, "byte*", 0x01)
EndFunc ; <== Level

Func _Agent_Team($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x010D, "byte*", 0x01)
EndFunc ; <== Team

Func _Agent_unknown14a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x010E, "word*", 0x02)
EndFunc ; <== unknown14a

Func _Agent_unknown14b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0110, "float*", 0x04)
EndFunc ; <== unknown14b

Func _Agent_EnergyPips($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0114, "float*", 0x04)
EndFunc ; <== EnergyPips

Func _Agent_unknown($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0118, "float*", 0x04)
EndFunc ; <== unknown

Func _Agent_EnergyPercent($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x011C, "float*", 0x04)
EndFunc ; <== EnergyPercent

Func _Agent_MaxEnergy($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0120, "long*", 0x04)
EndFunc ; <== MaxEnergy

Func _Agent_unknown15($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0124, "float*", 0x04)
EndFunc ; <== unknown15

Func _Agent_HPPips($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0128, "float*", 0x04)
EndFunc ; <== HPPips

Func _Agent_unknown16($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x012C, "float*", 0x04)
EndFunc ; <== unknown16

Func _Agent_HP($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0130, "float*", 0x04)
EndFunc ; <== HP

Func _Agent_MaxHP($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0134, "long*", 0x04)
EndFunc ; <== MaxHP

Func _Agent_Effects($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0138, "long*", 0x04)
EndFunc ; <== Effects

Func _Agent_unknown17($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x013C, "dword*", 0x04)
EndFunc ; <== unknown17

Func _Agent_unknown18a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0140, "ptr*", 0x04)
EndFunc ; <== unknown18a

Func _Agent_unknown18b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0144, "ptr*", 0x04)
EndFunc ; <== unknown18b

Func _Agent_unknown18c($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0148, "float*", 0x04)
EndFunc ; <== unknown18c

Func _Agent_unknown18d($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x014C, "dword*", 0x04)
EndFunc ; <== unknown18d

Func _Agent_unknown18e($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0150, "float*", 0x04)
EndFunc ; <== unknown18e

Func _Agent_ModelState($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0154, "long*", 0x04)
EndFunc ; <== ModelState

Func _Agent_TypeMap($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0158, "long*", 0x04)
EndFunc ; <== TypeMap

Func _Agent_unknown19a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x015C, "dword*", 0x04)
EndFunc ; <== unknown19a

Func _Agent_unknown19b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0160, "dword*", 0x04)
EndFunc ; <== unknown19b

Func _Agent_unknown19c($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0164, "dword*", 0x04)
EndFunc ; <== unknown19c

Func _Agent_unknown19d($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0168, "float*", 0x04)
EndFunc ; <== unknown19d

Func _Agent_InSpiritRange($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x016C, "long*", 0x04)
EndFunc ; <== InSpiritRange

Func _Agent_unknown20a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0170, "dword*", 0x04)
EndFunc ; <== unknown20a

Func _Agent_unknown20b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0174, "dword*", 0x04)
EndFunc ; <== unknown20b

Func _Agent_unknown20c($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0178, "dword*", 0x04)
EndFunc ; <== unknown20c

Func _Agent_unknown20d($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x017C, "dword*", 0x04)
EndFunc ; <== unknown20d

Func _Agent_LoginNumber($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0180, "long*", 0x04)
EndFunc ; <== LoginNumber

Func _Agent_ModelMode($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0184, "float*", 0x04)
EndFunc ; <== ModelMode

Func _Agent_unknown21a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0188, "dword*", 0x04)
EndFunc ; <== unknown21a

Func _Agent_ModelAnimation($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x018C, "long*", 0x04)
EndFunc ; <== ModelAnimation

Func _Agent_unknown22a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0190, "dword*", 0x04)
EndFunc ; <== unknown22a

Func _Agent_unknown22b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0194, "dword*", 0x04)
EndFunc ; <== unknown22b

Func _Agent_unknown22c($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x0198, "dword*", 0x04)
EndFunc ; <== unknown22c

Func _Agent_unknown22d($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x019C, "dword*", 0x04)
EndFunc ; <== unknown22d

Func _Agent_unknown22e($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01A0, "dword*", 0x04)
EndFunc ; <== unknown22e

Func _Agent_unknown22f($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01A4, "dword*", 0x04)
EndFunc ; <== unknown22f

Func _Agent_unknown22g($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01A8, "dword*", 0x04)
EndFunc ; <== unknown22g

Func _Agent_unknown22h($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01AC, "dword*", 0x04)
EndFunc ; <== unknown22h

Func _Agent_LastStrike($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01B0, "byte*", 0x01)
EndFunc ; <== LastStrike

Func _Agent_Allegiance($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01B1, "byte*", 0x01)
EndFunc ; <== Allegiance

Func _Agent_WeaponType($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01B2, "word*", 0x02)
EndFunc ; <== WeaponType

Func _Agent_Skill($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01B4, "word*", 0x02)
EndFunc ; <== Skill

Func _Agent_unknown23a($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01B6, "word*", 0x02)
EndFunc ; <== unknown23a

Func _Agent_unknown23b($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01B8, "byte*", 0x01)
EndFunc ; <== unknown23b

Func _Agent_unknown23c($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01B9, "byte*", 0x01)
EndFunc ; <== unknown23c

Func _Agent_WeaponItemId($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01BA, "word*", 0x02)
EndFunc ; <== WeaponItemId

Func _Agent_OffhandItemId($hProcess, $hAgent)
	Return _GWCA_Read($hProcess, $hAgent + 0x01BC, "word*", 0x02)
EndFunc ; <== OffhandItemId
