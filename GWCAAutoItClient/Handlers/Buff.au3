#include-once

; GWCAComm _Buff_ Handlers
; Auto Generated


Func _Buff_SkillId($hProcess, $hBuff)
	Return _GWCA_Read($hProcess, $hBuff + 0x0000, "long*", 0x04)
EndFunc ; <== SkillId

Func _Buff_unknown1($hProcess, $hBuff)
	Return _GWCA_Read($hProcess, $hBuff + 0x0004, "dword*", 0x04)
EndFunc ; <== unknown1

Func _Buff_BuffId($hProcess, $hBuff)
	Return _GWCA_Read($hProcess, $hBuff + 0x0008, "long*", 0x04)
EndFunc ; <== BuffId

Func _Buff_TargetId($hProcess, $hBuff)
	Return _GWCA_Read($hProcess, $hBuff + 0x000C, "long*", 0x04)
EndFunc ; <== TargetId
