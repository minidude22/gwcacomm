﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWCA
{
    public enum CMD
    {
        HEARTBEAT,
        DISCONNECT_PIPE_CONNECTION,
        UNLOAD_SERVER,

        REQUEST_BASE_POINTERS,

        MOVE,
        USE_SKILL,
        GET_SKILL_RECHARGE,

        GET_EFFECT_TIME_REMAINING,

        SEND_CHAT,

        GET_PLAYER,
        GET_AGENT_BY_ID,
        GET_AGENTS_POS,

    };

    public enum Target
    {
        Self = -2,
        Current = -1
    };

    public struct PseudoAgent
    {
        Int32 id;
        float rotation;
        Int32 nameproperties;
        float x;
        float y;
        UInt32 zplane;
        Int32 type;
        float movex;
        float movey;
        UInt32 extratype;
        UInt16 playernumber;
        byte primary;
        byte secondary;
        byte level;
        byte teamid;
        float energy;
        UInt32 maxenergy;
        float hppips;
        float hp;
        UInt32 maxhp;
        UInt32 effects;
        byte hex;
        UInt32 modelstate;
        UInt32 typemap;
        UInt32 inspiritrange;
        UInt32 loginnumber;
        UInt16 allegiance;
        UInt16 skill;
    }


    namespace Requests
    {
        public class Heartbeat
        {
            readonly CMD opcode = CMD.HEARTBEAT;
        }

        public class DisconnectPipe
        {
            readonly CMD opcode = CMD.DISCONNECT_PIPE_CONNECTION;
        }

        public class UnloadServer
        {
            readonly CMD opcode = CMD.UNLOAD_SERVER;
        }

        public class Move
        {
            readonly CMD opcode = CMD.MOVE;
            public float x;
            public float y;
            public UInt32 zplane;
        }

        public class UseSkill
        {
            readonly CMD opcode = CMD.USE_SKILL;
            public UInt32 slot;
            public UInt32 target;
            public Int32 calltarget;
        }

        public class GetSkillRecharge
        {
            readonly CMD opcode = CMD.GET_SKILL_RECHARGE;
            public UInt32 slot;
        }

        public class GetEffectTimeRemaining
        {
            readonly CMD opcode = CMD.GET_EFFECT_TIME_REMAINING;
            public UInt32 skillid;
        }

        public class SendChat
        {
            readonly CMD opcode = CMD.SEND_CHAT;
            public char channel;
            public string message;
        }

        public class BasePointers
        {
            public readonly CMD opcode = CMD.REQUEST_BASE_POINTERS;
        }

        public class GetAgentByID
        {
            readonly CMD opcode = CMD.GET_AGENT_BY_ID;
            public int id;
        }

        public class GetPlayer
        {
            readonly CMD opcode = CMD.GET_PLAYER;
        }

        public class GetAgentPosStream
        {
            readonly CMD opcode = CMD.GET_AGENTS_POS;
        }
    }

    namespace Replies
    {
        public class Heartbeat
        {
            public UInt32 reply;
        }

        public class GetSkillRecharge
        {
            public UInt32 isrecharged;
        }

        public class GetEffectTimeRemaining
        {
            public Int32 timeremaining;
        }

        public class BasePointers
        {
            public UIntPtr baseptr;
            public UIntPtr agarray;
            public UIntPtr mapidptr;
            public UIntPtr skillconstptr;
        }

        public class GetAgentById
        {
            public PseudoAgent agent;
        }

        public class GetPlayer
        {
            public PseudoAgent agent;
        }

        // works as follows: first send request, then send
        // multiple replies, each with id and position of an agent
        // reply stream ends when id = -1
        public class GetAgentPosStream
        {
           public Int32 id;
           public float x;
           public float y;
        }

    }

}
