﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Pipes;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

namespace GWCA
{
    public class GWCACommunicator
    {
        protected Process process;
        protected NamedPipeClientStream GWCAPipe = null;
        protected byte[] outbuffer;
        protected byte[] inbuffer;

        #region Constructors
        public GWCACommunicator(Process proc)
        {
            process = proc;
            GWCAPipe = new NamedPipeClientStream(
                ".", 
                "GWComm_" + proc.Id, 
                PipeDirection.InOut, 
                PipeOptions.None
                );

            GWCAPipe.Connect(1000);

            outbuffer = new byte[1024];
            inbuffer = new byte[1024];
        }

        public GWCACommunicator(Process proc,string ipv4address)
        {
            process = proc;
            GWCAPipe = new NamedPipeClientStream(
                ipv4address,
                "GWComm_" + proc.Id, 
                PipeDirection.InOut, 
                PipeOptions.None
                );

            GWCAPipe.Connect(1000);

            outbuffer = new byte[1024];
            inbuffer = new byte[1024];
        }

        public GWCACommunicator(string windowtitle)
        {
            Process[] procs = Process.GetProcessesByName("Gw");

            foreach(Process proc in procs)
            {
                if(proc.MainWindowTitle != "Guild Wars" 
                && proc.MainWindowTitle == windowtitle)
                {
                    process = proc;
                    GWCAPipe = new NamedPipeClientStream(
                      ".",
                     "GWComm_" + proc.Id,
                     PipeDirection.InOut,
                     PipeOptions.None
                     );

                    GWCAPipe.Connect(1000);

                    outbuffer = new byte[1024];
                    inbuffer = new byte[1024];
                }
            }
        }

        public GWCACommunicator(string windowtitle,string ipv4address)
        {
            Process[] procs = Process.GetProcessesByName("Gw");

            foreach (Process proc in procs)
            {
                if (proc.MainWindowTitle != "Guild Wars"
                && proc.MainWindowTitle == windowtitle)
                {
                    process = proc;
                    GWCAPipe = new NamedPipeClientStream(
                      ipv4address,
                     "GWComm_" + proc.Id,
                     PipeDirection.InOut,
                     PipeOptions.None
                     );

                    GWCAPipe.Connect(1000);

                    outbuffer = new byte[1024];
                    inbuffer = new byte[1024];
                }
            }
        }
        #endregion
     

        public void Send<T>(T structure)
        {
            if (!GWCAPipe.IsConnected) return;

            GWCAPipe.WaitForPipeDrain();

            int structsize = Marshal.SizeOf(typeof(T));
            

            IntPtr ptr = Marshal.AllocHGlobal(structsize);
            Marshal.StructureToPtr(structure,ptr,true);
            Marshal.Copy(ptr, outbuffer, 0, structsize);
            Marshal.FreeHGlobal(ptr);


            GWCAPipe.Write(outbuffer, 0, structsize);
        }

        public T Recv<T>()
        {

            if (!GWCAPipe.IsConnected) return default(T);

            int structsize = Marshal.SizeOf(typeof(T));

            while(GWCAPipe.Read(inbuffer, 0, structsize) == 0)
            {
                Thread.Sleep(150);
            }

            IntPtr buf = Marshal.AllocHGlobal(structsize);
            Marshal.Copy(inbuffer, 0, buf, structsize);
            T ret = Marshal.PtrToStructure<T>(buf);
            Marshal.FreeHGlobal(buf);

            return ret;
        }
        public T2 Request<T1,T2>(T1 request)
        {
            Send<T1>(request);
            return Recv<T2>();
        }
    }
}
