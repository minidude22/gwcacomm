﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using GWCA.Memory;

namespace GWCA
{
    public class GWCAClient : GWCACommunicator
    {
        public GWCAMemory memory;
        public Replies.BasePointers baseptrs;

        #region Constructors
        public GWCAClient(Process proc)
           : base(proc)
       {
           memory = new GWCAMemory(process);

           Send<Requests.BasePointers>(new Requests.BasePointers());
           baseptrs = Recv<Replies.BasePointers>();
       }

       public GWCAClient(string wintitle)
           : base(wintitle)
       {
           memory = new GWCAMemory(process);

           Send<Requests.BasePointers>(new Requests.BasePointers());
           baseptrs = Recv<Replies.BasePointers>();
       }

       public GWCAClient(Process proc,string ipv4address)
           : base(proc, ipv4address)
       {
           memory = new GWCAMemory(process);

           Send<Requests.BasePointers>(new Requests.BasePointers());
           baseptrs = Recv<Replies.BasePointers>();
       }

       public GWCAClient(string wintitle, string ipv4address)
           : base(wintitle, ipv4address)
       {
           memory = new GWCAMemory(process);

           Send<Requests.BasePointers>(new Requests.BasePointers());
           baseptrs = Recv<Replies.BasePointers>();
       }
        #endregion


    }
}
